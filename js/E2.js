function calcular(){
    var grados = parseInt(document.getElementById('grados').value);
    var opt = document.getElementById('opt').value;

    if (opt === "c") {
        var res = (9/5*grados)+32;
    } else if (opt === "f") {
        var res = (grados-32)*(5/9);
    }
    document.getElementById('res').value = res
}

function limpiar(){
    document.getElementById('res').value = "";
    document.getElementById('grados').value = "";
}