function generar(){
    var select = document.getElementById("edades");
    var bebe = 0;
    var nino = 0;
    var adolescente = 0;
    var adulto = 0;
    var anciano = 0;
    while (select.options.length > 0) {
      select.remove(0);
    } 
        for (var i = 0; i < 100; i++) {
             var numeroAleatorio = Math.floor(Math.random() * 90) + 1;

             var option = document.createElement("option");
              option.value = numeroAleatorio;
              option.text = numeroAleatorio;

              select.appendChild(option);

              if (numeroAleatorio <= 3) {
                bebe++;
              } else if (numeroAleatorio <= 12) {
                nino++;
              } else if (numeroAleatorio <= 17) {
                adolescente++;
              } else if (numeroAleatorio <= 60) {
                adulto++;
              } else if (numeroAleatorio <= 100) {
                anciano++;
              } 
         }
    document.getElementById('bebe').textContent = bebe;
    document.getElementById('nino').textContent = nino;
    document.getElementById('adolescente').textContent = adolescente; 
    document.getElementById('adulto').textContent = adulto;
    document.getElementById('anciano').textContent = anciano;  
}

function limpiar(){
  var select = document.getElementById("edades");
  while (select.options.length > 0) {
    select.remove(0);
  } 
  document.getElementById('bebe').textContent = "";
  document.getElementById('nino').textContent = "";
  document.getElementById('adolescente').textContent = ""; 
  document.getElementById('adulto').textContent = "";
  document.getElementById('anciano').textContent = "";  
}