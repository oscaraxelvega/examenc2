function calcular(){
    var viaje = parseInt(document.getElementById('tipoviaje').value);
    var precio = parseInt(document.getElementById('precio').value);

    if (viaje === 2) {
        precio = precio*1.80;
    }

    document.getElementById('subtotal').value = precio;
    var impuesto = precio * 0.16;
    document.getElementById('impuesto').value = impuesto;
    var total = precio * 1.16;
    document.getElementById('total').value = total;
}

function limpiar(){
    document.getElementById('subtotal').value = "";
    document.getElementById('impuesto').value = "";
    document.getElementById('total').value = "";
    document.getElementById('tipoviaje').value = "";
    document.getElementById('precio').value = "";
    document.getElementById('destino').value = "";
    document.getElementById('cliente').value = "";
    document.getElementById('numboleto').value = "";
}